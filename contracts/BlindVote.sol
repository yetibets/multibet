pragma solidity ^0.4.10;

contract BlindVote {

    // Struct for holding the contents of a blinded vote.
    // The bytes32 blindedVote member represents:
    //  keccak256(vote, fake, secret)
    struct BlindedVote {
        bytes32 blindedVote;
        uint deposit;
    }

    // Struct for holding the contents of the actual vote
    // Used while in the revealing vote state.
    struct ActualVote {
        address voterAddress;
        uint8 voteOption;
        uint deposit;
    }

    enum State {
        Initialized, // When the contract is first instantiated.
        VotingStarted, // When a particular vote has started.
        VotingEnded, // Voting has ended, voters can reveals their votes.
        VotesRevealed // Revealing of votes has ended, payouts can occur.
    }

    address public owner;
    address public houseAddress;

    State public state;

    // Stores the available voting options and their total collected bets.
    uint[] public votingOptions;

    // Stores all blinded votes. We don't know if these are legitimate or not until
    // the voting period has ended and we will iterate over all of them.
    mapping(address => BlindedVote[]) public blindedVotes;

    // Stores all addresses of blinded votes in order to be able to clean up the 
    // mapping when we create a new BlindVote.
    address[] public blindedVoteAddresses;

    // Stores all of the actual votes for each option (key = each vote option).
    // This allows us to figure out what proportion of each vote/deposit was
    // compared to the total (allowing to to provide balanced payouts for each winning vote).
    mapping(uint8 => ActualVote[]) public actualVotesByOption;

    // Allowed withdrawals of previous votes
    mapping(address => uint) pendingReturns;

    // Pending payouts
    mapping(address => uint) pendingPayouts;

    modifier onlyOwner() { require(msg.sender == owner); _; }
    modifier onlyState(State _state) { require(state == _state); _; }

    function BlindVote(address _houseAddress) {
        owner = msg.sender;
        houseAddress = _houseAddress;
        state = State.Initialized;
    }

    // Kicks off the ability to vote.
    // Cleans up any state left by the last run of this contract.
    function startVote(uint8 options) onlyOwner {

        require(state == State.Initialized || state == State.VotesRevealed);

        require(options > 1);

        // If we're not in the initialized state we need to clear the state left over
        // from the last voting session.
        if (state == State.VotesRevealed) {
            for (uint i = 0; i < blindedVoteAddresses.length; i++) {
                delete blindedVotes[blindedVoteAddresses[i]];
            }

            for (uint8 j = 0; j < votingOptions.length; j++) {
                delete actualVotesByOption[j];
            }

            delete votingOptions;
        }

        for (uint8 option = 0; option < options; option++) {
            votingOptions.push(0);
        }

        state = State.VotingStarted;
    }

    // Ends the ability to vote.
    function endVoting() onlyOwner onlyState(State.VotingStarted) {
        state = State.VotingEnded;
    }

    // Ends the vote reveal period.
    function endReveals() onlyOwner onlyState(State.VotingEnded) {
        state = State.VotesRevealed;

        finalizeVote();
    }

    /// End the vote
    function finalizeVote() onlyState(State.VotesRevealed) onlyOwner internal
    {   
        uint8 mostVoted = getHighestOption();

        uint amountToDistribute = 0;

        // Gather up all the funds deposited on the losers.
        for (uint8 i = 0; i < votingOptions.length; i++) {
            if (i == mostVoted)
                continue;

            amountToDistribute += votingOptions[i];    
        }

        // Distribute the winnings across the pending payouts for each 
        // address that voted (one or more times) on the winner.
        distributeWinnings(amountToDistribute, mostVoted);
    }

    /// Place a blinded bid with `_blindedVote` = keccak256(vote,
    /// fake, secret).
    function vote(bytes32 _blindedVote)
        payable
        onlyState(State.VotingStarted)
    {
        require(msg.value > 0);

        blindedVotes[msg.sender].push(BlindedVote({
            blindedVote: _blindedVote,
            deposit: msg.value
        }));

        blindedVoteAddresses.push(msg.sender);
    }

    /// Reveal your blinded votes. You will get a refund for all
    /// correctly blinded invalid votes and for all votes except for
    /// the totally highest.
    function reveal(
        uint8[] _votes,
        bool[] _fake,
        bytes32[] _secret
    )
        onlyState(State.VotingEnded)
    {
        uint length = blindedVotes[msg.sender].length;
        require(_votes.length == length);
        require(_fake.length == length);
        require(_secret.length == length);

        uint refund;
        for (uint i = 0; i < length; i++) {

            var blindVote = blindedVotes[msg.sender][i];

            var (voteOption, fake, secret) = (_votes[i], _fake[i], _secret[i]);

            if (blindVote.blindedVote != keccak256(voteOption, fake, secret)) {
                // Bid was not actually revealed.
                // Do not refund deposit.
                continue;
            }

            refund += blindVote.deposit;

            // If the vote was 'real', place the vote.
            if (!fake) {

                // If we successfully placed the vote do not refund the value sent.
                if (placeVote(msg.sender, blindVote.deposit, voteOption))
                    refund -= blindVote.deposit;
            }

            // Make it impossible for the sender to re-claim
            // the same deposit.
            blindVote.blindedVote = 0;
        }

        // Refund the original sender for any of their fake, or invalid, votes.
        msg.sender.transfer(refund);
    }

    function placeVote(address voter, uint voteValue, uint8 voteOption) internal
        returns (bool success)
    {
        if (voteOption > votingOptions.length - 1 || voteOption < 0) {
            return false;
        }

        // No need to test voteValue, this was checked in the original vote function.

        votingOptions[voteOption] += voteValue;

        actualVotesByOption[voteOption].push(ActualVote({
            voterAddress: voter,
            voteOption: voteOption,
            deposit: voteValue
        }));

        return true;
    }

    // Distributes winnings to the pendingPayouts mapping.
    function distributeWinnings(uint amountToDistribute, uint8 winningOption) internal {

        // 10% to house
        uint houseCut = amountToDistribute / 10;

        amountToDistribute -= houseCut;

        // Iterate through all ActualVotes for the winning vote.
        for (uint8 i = 0; i < actualVotesByOption[winningOption].length; i++) {

            ActualVote actualVote = actualVotesByOption[winningOption][i];

            uint proportionOfWinningValue = actualVote.deposit / amountToDistribute;

            pendingPayouts[actualVote.voterAddress] += amountToDistribute * proportionOfWinningValue;
        }

        houseAddress.transfer(houseCut);
    }

    // Pays out all pending payouts for the sender.
    function getPayout() {
        require(pendingPayouts[msg.sender] > 0);

        uint payout = pendingPayouts[msg.sender];
        pendingPayouts[msg.sender] = 0;

        msg.sender.transfer(payout);
    }

    // Returns the highest voted option.
    function getHighestOption() internal returns (uint8) {
        uint highestValue = 0;
        uint8 mostVoted;
        
        for (uint8 i = 0; i < votingOptions.length; i++) {
            if (votingOptions[i] > highestValue) {
                highestValue = votingOptions[i];
                mostVoted = i;
            }
        }

        return mostVoted;
    }
}
